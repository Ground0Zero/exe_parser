<?php
include ('simple_html_dom-master/simple_html_dom.php');

$html= file_get_html('https://www.kickstarter.com/projects/pugz/pugz-worlds-smallest-wireless-earbuds-you-charge-w/description');


$pledgers = array();
$pledgers_info = array();
foreach($html->find('.pledge__info') as $pledger) {
	$element['amount']  = @$pledger->find('.pledge__amount', 0)->plaintext;
	$element['backer']  = @$pledger->find('.pledge__backer-count', 0)->plaintext;
	$element['limit']   = @$pledger->find('.pledge__limit', 0)->plaintext;
	
	//Remove about from amount to output clean amount
	$element['amount'] = strstr ($element['amount'], 'About', true);
	//Remove limit from backer to output clean backer
	$element['backer']	= str_replace($element['limit'], '', $element['backer']);
	$pledgers_info[] = $element;
}

// Uncoment to get array list
//echo '<pre>',print_r($pledgers_info),'</pre>'; exit;

$json_data	= json_encode($pledgers_info);
$infos 		= json_decode($json_data, true); 

//puts data in pledgers.txt file
file_put_contents('pledgers.txt', json_encode($pledgers_info));

//echo '<pre>',print_r($infos),'</pre>'; exit;

$i = 1;
foreach($infos as $info) {
	echo $i++.'.) '.$info['amount'].', '.$info['backer'].', '.$info['limit'].'<br />';
}

$html->clear();
unset($html);

?>